package eu.mvp.helloworld;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telecom.Call;
import android.util.Log;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebSettings;
import android.webkit.WebView;

import java.util.Timer;
import java.util.TimerTask;

import static eu.mvp.helloworld.R.id.WWW;

public class MainActivity extends AppCompatActivity
{
WebView Nav ;

void CallJavascript(String code)
{
	if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT)
	{
		Nav.evaluateJavascript(code, null);
	}
	else
	{
		StringBuilder js=new StringBuilder() ;
		js.append("javascript:").append(code) ;
		Nav.loadUrl(js.toString());
	}
}

@JavascriptInterface
public String fnOne(String x)
{
		StringBuilder cmd=new StringBuilder() ;
		cmd.append("Traitement effectue en Java pour ").append(x) ;
/*
		Nav.evaluateJavascript(cmd.toString(), new ValueCallback<String>()
		 
		{
			@Override
			public void onReceiveValue(String value)
			{
				
			}
		});
	*/
	return cmd.toString() ;
}

private Handler handler = new Handler()
{
	@Override
	public void handleMessage(Message msg)
	{
		switch (msg.what)
		{
			case 1:
				CallJavascript("fnJS1('Envoi periodique par Java')") ;
				// rappel periodique
				Message msge=handler.obtainMessage(1);
				handler.sendMessageDelayed(msge, 2000);
				break;
		}
	}
} ;

@Override
protected void onCreate(Bundle savedInstanceState)
{
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_main);
	
	getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN) ;
	Nav=(WebView)findViewById(WWW) ;
	Nav.getSettings().setDomStorageEnabled(true) ;
	Nav.getSettings().setJavaScriptEnabled(true) ;
	Nav.addJavascriptInterface(this, "MON_INTERFACE") ;

	Nav.setWebContentsDebuggingEnabled(true) ;
	Nav.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE) ;
	Nav.loadUrl("file:///android_asset/index.html");
	
	// petite tache repetitive qui agit sur le HTML
	Message msge=handler.obtainMessage(1);
	handler.sendMessageDelayed(msge, 1000);
	
}

}
